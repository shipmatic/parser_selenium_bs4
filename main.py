import time
from selenium_cl import selenium_google_form
import requests
from bs4 import BeautifulSoup



PARSING_LINK = 'https://www.zillow.com/homes/for_rent/1-_beds/?searchQueryState=%7B%22pagination%22%3A%7B%7D%2C%22mapBounds%22%3A%7B%22west%22%3A-123.09113539648438%2C%22east%22%3A-121.77552260351563%2C%22south%22%3A37.20595871710531%2C%22north%22%3A38.340273380322785%7D%2C%22isMapVisible%22%3Atrue%2C%22filterState%22%3A%7B%22price%22%3A%7B%22max%22%3A872627%7D%2C%22beds%22%3A%7B%22min%22%3A1%7D%2C%22pmf%22%3A%7B%22value%22%3Afalse%7D%2C%22fore%22%3A%7B%22value%22%3Afalse%7D%2C%22mp%22%3A%7B%22max%22%3A3000%7D%2C%22auc%22%3A%7B%22value%22%3Afalse%7D%2C%22nc%22%3A%7B%22value%22%3Afalse%7D%2C%22fr%22%3A%7B%22value%22%3Atrue%7D%2C%22fsbo%22%3A%7B%22value%22%3Afalse%7D%2C%22cmsn%22%3A%7B%22value%22%3Afalse%7D%2C%22pf%22%3A%7B%22value%22%3Afalse%7D%2C%22fsba%22%3A%7B%22value%22%3Afalse%7D%7D%2C%22isListVisible%22%3Atrue%7D'
GFORM_URL = 'https://docs.google.com/forms/d/e/1FAIpQLSembsIbLDkkBab3RkHFRmN1JAUzspqCuPAX63sAKv2m-8Mo9w/viewform?usp=sf_link'
header = {
'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 11_4_0) AppleWebKit/537.36 (KHTML, like Gecko)  Chrome/89.0.4389.114 Safari/537.36',
'Accept-Language':'ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7,uk;q=0.6,de;q=0.5'
}



response = (requests.get(PARSING_LINK, headers=header)).text
soup = BeautifulSoup(response, 'html.parser')

data = soup.find_all('div', class_='list-card-info')

results = []

for line in data:
    web_link = line.find('a',  class_="list-card-link list-card-link-top-margin")
    price = line.find('div', class_="list-card-price")
    try:
        print(web_link.getText())
        print(web_link.get('href'))
        print(price.getText())
        results.append({
            'address':web_link.getText(),
            'web': web_link.get('href'),
            'price':price.getText(),

            })
    except:
        pass


bot = selenium_google_form()
bot.fill_form(results)



