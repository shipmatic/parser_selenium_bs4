import time
from selenium import webdriver

chrome_driver_path = '/Users/SSP_TECH/Documents/Programming/chromedriver'
driver = webdriver.Chrome(executable_path=chrome_driver_path)
GFORM_URL = 'https://docs.google.com/forms/d/e/1FAIpQLSembsIbLDkkBab3RkHFRmN1JAUzspqCuPAX63sAKv2m-8Mo9w/viewform?usp=sf_link'

class selenium_google_form():
    def __init__(self) -> None:
        self.driver = driver
    
    def fill_form(self, results):
        self.driver.get(GFORM_URL)   
        for result in results:
            time.sleep(1)                                 
            self.input_address = self.driver.find_element_by_css_selector('#mG61Hd > div.freebirdFormviewerViewFormCard.exportFormCard > div > div.freebirdFormviewerViewItemList > div:nth-child(1) > div > div > div.freebirdFormviewerComponentsQuestionTextRoot > div > div.quantumWizTextinputPaperinputMainContent.exportContent > div > div.quantumWizTextinputPaperinputInputArea > input')
            self.input_link = self.driver.find_element_by_css_selector('#mG61Hd > div.freebirdFormviewerViewFormCard.exportFormCard > div > div.freebirdFormviewerViewItemList > div:nth-child(2) > div > div > div.freebirdFormviewerComponentsQuestionTextRoot > div > div.quantumWizTextinputPaperinputMainContent.exportContent > div > div.quantumWizTextinputPaperinputInputArea > input')
            self.input_price = self.driver.find_element_by_css_selector('#mG61Hd > div.freebirdFormviewerViewFormCard.exportFormCard > div > div.freebirdFormviewerViewItemList > div:nth-child(3) > div > div > div.freebirdFormviewerComponentsQuestionTextRoot > div > div.quantumWizTextinputPaperinputMainContent.exportContent > div > div.quantumWizTextinputPaperinputInputArea > input')
            self.button_submit = self.driver.find_element_by_xpath('//*[@id="mG61Hd"]/div[2]/div/div[3]/div[1]/div/div')
            
            time.sleep(1)
            self.input_address.send_keys(result['address'])
            self.input_link.send_keys(result['web'])
            self.input_price.send_keys(result['price'])
            self.button_submit.click()
            time.sleep(1)
            self.button_one_more = self.driver.find_element_by_xpath('/html/body/div[1]/div[2]/div[1]/div/div[4]/a')
            self.button_one_more.click()
        self.driver.quit()



